package org.semanticweb.ore.interfacing;

import org.semanticweb.ore.utilities.FilePathString;

public class DefaultReasonerDescriptionFactory implements ReasonerDescriptionFactory {
	
	public ReasonerDescription createReasonerDescription(String reasonerName, ReasonerInterfaceType reasonerInterfaceType, FilePathString startingScript, FilePathString workingDirectory, OntologyFormatType ontologyFormatType, ExpressivitySupport expressivitySupport, String outputPathString) {
		return new ReasonerDescription(reasonerName,reasonerInterfaceType,startingScript,workingDirectory,ontologyFormatType,expressivitySupport,outputPathString);
	}

}
