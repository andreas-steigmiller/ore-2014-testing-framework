package org.semanticweb.ore.configuration;

public enum ConfigType {
	
	
	CONFIG_TYPE_EXECUTION_TIMEOUT("org.semanticweb.ore.execution.timeout"),
	CONFIG_TYPE_BASE_DIRECTORY("org.semanticweb.ore.directory.base"),
	CONFIG_TYPE_QUERIES_DIRECTORY("org.semanticweb.ore.directory.queries"),
	CONFIG_TYPE_RESPONSES_DIRECTORY("org.semanticweb.ore.directory.responses"),
	CONFIG_TYPE_CONVERSIONS_DIRECTORY("org.semanticweb.ore.directory.conversions"),
	CONFIG_TYPE_EXPECTATIONS_DIRECTORY("org.semanticweb.ore.directory.expectations"),
	CONFIG_TYPE_ONTOLOGIES_DIRECTORY("org.semanticweb.ore.directory.ontologies"),

	CONFIG_TYPE_RESPONSES_DATE_SUB_DIRECTORY("org.semanticweb.ore.responses.datesubdirectory"),
	CONFIG_TYPE_WRITE_NORMALISED_RESULTS("org.semanticweb.ore.responses.writenormalisedresults"),
	CONFIG_TYPE_SAVE_LOAD_RESULT_HASH_CODES("org.semanticweb.ore.responses.saveloadresulthashcodes"),
	
	CONFIG_TYPE_EXECUTION_ADD_TIMEOUT_AS_FIRST_ARGUMENT("org.semanticweb.ore.execution.addtimeoutasfirstargument");

	
	private String mConfigTypeString = null;
	
	private ConfigType(String typeString) {
		mConfigTypeString = typeString;
	}
	
	public String getConfigTypeString() {
		return mConfigTypeString;
	}
	

}
