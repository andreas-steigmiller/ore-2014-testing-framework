package org.semanticweb.ore.execution.events;

import org.semanticweb.ore.configuration.Config;
import org.semanticweb.ore.execution.ReasonerQueryExecutedCallback;
import org.semanticweb.ore.interfacing.ReasonerDescription;
import org.semanticweb.ore.querying.Query;
import org.semanticweb.ore.threading.Event;

public class ReasonerProcessInitialiseEvent implements Event {
	
	private ReasonerDescription mReasonerDescription = null;
	private Config mConfig = null;
	private Query mQuery = null;
	private ReasonerQueryExecutedCallback mCallback = null;
	private String mResponseDestinationString = null;
	

	public ReasonerDescription getReasonerDescription() {
		return mReasonerDescription;
	}
	
	public Config getConfig() {
		return mConfig;
	}
	
	public Query getQuery() {
		return mQuery;
	}
	
	public ReasonerQueryExecutedCallback getCallback() {
		return mCallback;
	}	
	
	public String getResponseDestinationString() {
		return mResponseDestinationString;
	}
	
	public ReasonerProcessInitialiseEvent(Query query, ReasonerDescription reasoner, String responseDestinationString, Config config, ReasonerQueryExecutedCallback callback) {
		mReasonerDescription = reasoner;
		mConfig = config;
		mQuery = query;
		mCallback = callback;
		mResponseDestinationString = responseDestinationString;
	}

}
