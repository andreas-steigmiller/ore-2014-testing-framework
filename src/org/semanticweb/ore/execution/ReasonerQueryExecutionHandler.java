package org.semanticweb.ore.execution;

import java.io.File;
import java.io.IOException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.exec.ShutdownHookProcessDestroyer;
import org.semanticweb.ore.configuration.Config;
import org.semanticweb.ore.configuration.ConfigDataValueReader;
import org.semanticweb.ore.configuration.ConfigType;
import org.semanticweb.ore.conversion.OntologyFormatRedirector;
import org.semanticweb.ore.execution.events.ReasonerProcessCompletedEvent;
import org.semanticweb.ore.execution.events.ReasonerProcessExecutionExceptionEvent;
import org.semanticweb.ore.execution.events.ReasonerProcessExecutionIOExceptionEvent;
import org.semanticweb.ore.execution.events.ReasonerProcessInitialiseEvent;
import org.semanticweb.ore.interfacing.ReasonerAdaptor;
import org.semanticweb.ore.interfacing.ReasonerAdaptorFactory;
import org.semanticweb.ore.interfacing.ReasonerDescription;
import org.semanticweb.ore.querying.Query;
import org.semanticweb.ore.querying.QueryResponse;
import org.semanticweb.ore.querying.QueryResponseStoringHandler;
import org.semanticweb.ore.threading.Event;
import org.semanticweb.ore.threading.EventThread;
import org.semanticweb.ore.utilities.FilePathString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReasonerQueryExecutionHandler extends EventThread {
	
	final private static Logger mLogger = LoggerFactory.getLogger(ReasonerQueryExecutionHandler.class);
	
	final protected static int mDefaultExecutionTimeout = 1000 * 60 * 5; // 5 minutes
	
	
	protected ReasonerAdaptorFactory mReasonerAdaptorFactory = null;	
	protected OntologyFormatRedirector mFormatRedirector = null;
	protected QueryResponseStoringHandler mResponseStoringHandler = null;
	
	protected Config mConfig = null;	
	protected Query mQuery = null;
	protected ExecuteWatchdog mWatchDog = null;
	protected DefaultExecutor mExecutor = null;
	protected ReasonerAdaptor mReasonerAdaptor = null;
	protected ReasonerDescription mReasoner = null;
	
	protected ReasonerQueryExecutionReport mExecutionReport = null;
	protected boolean mExecutionActive = false;
	protected ReasonerQueryExecutedCallback mCallback = null;
	
	protected long mStartReasonerExecutionTime = 0;
	protected long mFinishReasonerExecutionTime = 0;
	
	protected long mExecutionTimeout = 0;
	


	public void executeReasonerQuery(ReasonerDescription reasoner, Query query, String responseDestinationString, Config config, ReasonerQueryExecutedCallback callback) {
		postEvent(new ReasonerProcessInitialiseEvent(query, reasoner, responseDestinationString, config, callback));
	}
	

	public QueryResponse executeReasonerQuery(ReasonerDescription reasoner, Query query, String responseDestinationString, Config config) {
		QueryResponse queryResponse = null;
		ReasonerQueryExecutedBlockingCallback callback = new ReasonerQueryExecutedBlockingCallback();
		executeReasonerQuery(reasoner,query,responseDestinationString,config,callback);
		try {
			callback.waitForCallback();
			queryResponse = callback.getQueryResponse();
		} catch (InterruptedException e) {
			mLogger.warn("Waiting for execution of query '{}' for reasoner '{}' interrupted.",query.toString(),reasoner.toString());
		}
		return queryResponse;
	}
		
	
	public ReasonerQueryExecutionHandler(ReasonerAdaptorFactory reasonerAdaptorFactory, OntologyFormatRedirector formatRedirector, QueryResponseStoringHandler responseStoringHandler) {
		mReasonerAdaptorFactory = reasonerAdaptorFactory;
		mFormatRedirector = formatRedirector;
		mResponseStoringHandler = responseStoringHandler;
		startThread();
	}
	

	
	protected void threadStart() {
		super.threadStart();
	}	

	protected void threadFinish() {	
		super.threadFinish();
	}	
	
	
	protected boolean isExecutionActive() {
		return mExecutionActive;
	}
	
	
	protected boolean processEvent(Event e) {
		if (super.processEvent(e)) {
			return true;
		} else {
			if (e instanceof ReasonerProcessInitialiseEvent) {
				ReasonerProcessInitialiseEvent rpie = (ReasonerProcessInitialiseEvent)e;
				if (isExecutionActive()) {
					mLogger.error("Query execution already in progress, query '{}' for reasoner '{}' cannot be processed.",rpie.getQuery().toString(),rpie.getReasonerDescription().toString());
				} else {
					mCallback = rpie.getCallback();
					if (initReasonerExecution(rpie.getReasonerDescription(), rpie.getQuery(), rpie.getResponseDestinationString(), rpie.getConfig())) {
						executeReasoner(mReasonerAdaptor);
					}
				}
				return true;
			} else  if (e instanceof ReasonerProcessCompletedEvent) {	
				if (isExecutionActive()) {
					mExecutionReport.setTimedOut(mWatchDog.killedProcess());
					mExecutionReport.setExecutionCompleted(true);
					finishReasonerExecution();
				}
				return true;
			} else  if (e instanceof ReasonerProcessExecutionExceptionEvent) {	
				ReasonerProcessExecutionExceptionEvent rpeee = (ReasonerProcessExecutionExceptionEvent)e;
				if (isExecutionActive()) {
					mExecutionReport.setExecutionError(true);
					mExecutionReport.setTimedOut(mWatchDog.killedProcess());
					mExecutionReport.addErrorMessage(rpeee.getException().getMessage());
					finishReasonerExecution();
				}
				return true;
			} else  if (e instanceof ReasonerProcessExecutionIOExceptionEvent) {	
				ReasonerProcessExecutionIOExceptionEvent rpeioe = (ReasonerProcessExecutionIOExceptionEvent)e;
				if (isExecutionActive()) {
					mExecutionReport.setExecutionError(true);
					mExecutionReport.setTimedOut(mWatchDog.killedProcess());
					mExecutionReport.addErrorMessage(rpeioe.getException().getMessage());
					finishReasonerExecution();
				}
				return true;
			}
		}
		return false;
	}
	
	

	
	protected void finishReasonerExecution() {
		mFinishReasonerExecutionTime = System.currentTimeMillis();
		mExecutionReport.setExecutionTime(mFinishReasonerExecutionTime-mStartReasonerExecutionTime);
		
		mReasonerAdaptor.completeReasonerExecution();
		
		boolean failed = false;
		
		if (mExecutionReport.hasTimedOut()) {
			failed = true;
			mLogger.warn("Execution of query '{}' for reasoner '{}' timed out.", mQuery.toString(),mReasoner.toString());
		}
		
		
		if (mExecutionReport.hasExecutionError()) {
			failed = true;
			mLogger.error("Execution of query '{}' for reasoner '{}' failed, got error '{}'.", new Object[]{mQuery.toString(),mReasoner.toString(),mExecutionReport.getErrorString()});
		} 
		
		
		if (!failed) {
			mLogger.info("Successfully executed query '{}' for reasoner '{}'.", mQuery.toString(),mReasoner.toString());
		}
		
		QueryResponse response = mReasonerAdaptor.createQueryResponse(mExecutionReport);
		
		if (response != null) {
			mResponseStoringHandler.saveQueryResponseData(response);
		}
		
		if (mCallback != null) {
			mCallback.reasonerQueryExecuted(response);
		}
		
		mExecutionActive = false;		
	}
	
	
	protected boolean initReasonerExecution(ReasonerDescription reasoner, Query query, String responseDestinationString, Config config) {
		mExecutionActive = false;
		mConfig = config;
		mQuery = query;
		mReasoner = reasoner;
		
		
		mExecutionTimeout = ConfigDataValueReader.getConfigDataValueInteger(mConfig,ConfigType.CONFIG_TYPE_EXECUTION_TIMEOUT,mDefaultExecutionTimeout);		
		mWatchDog = new ExecuteWatchdog(mExecutionTimeout);
		
		mLogger.info("Preparing execution of query '{}' for reasoner '{}'.", mQuery.toString(),mReasoner.toString());
		
		mReasonerAdaptor = mReasonerAdaptorFactory.getReasonerAdapter(mReasoner, mQuery, responseDestinationString, mFormatRedirector);
		
		mExecutionReport = new ReasonerQueryExecutionReport();
		
		String responseFileString = mReasonerAdaptor.getResponseFileString();
		File responseFile = new File(responseFileString);
		if (responseFile.isFile() && responseFile.exists() && responseFile.length() > 0) {
			mLogger.info("Query response '{}' already exists, trying to load existing response.", responseFileString);
			QueryResponse loadedQueryResponse = mResponseStoringHandler.loadQueryResponseData(new FilePathString(responseFileString));
			if (loadedQueryResponse != null) {
				mLogger.info("Successfully loaded query response from file '{}' for reasoner '{}', skipping evaluation for query '{}'.", new Object[]{responseFileString,mReasoner.toString(),mQuery});
				if (mCallback != null) {
					mCallback.reasonerQueryExecuted(loadedQueryResponse);
				}
				return false;
			}

		}
		
		return true;
	}
	
	
	protected boolean executeReasoner(ReasonerAdaptor adpater) {
		mExecutionActive = true;
		
		// command to be executed
		CommandLine commandLine = new CommandLine(adpater.getReasonerExecutionCommandString());
		
		boolean addTimeoutAsFirstArgument = ConfigDataValueReader.getConfigDataValueBoolean(mConfig, ConfigType.CONFIG_TYPE_EXECUTION_ADD_TIMEOUT_AS_FIRST_ARGUMENT, false);
		if (addTimeoutAsFirstArgument) {	
			commandLine.addArgument(new Long(mExecutionTimeout).toString());
		}
		
		// adding its arguments
		commandLine.addArguments(adpater.getReasonerExecutionArguments().toArray(new String[0]));
		
		PumpStreamHandler streamHandler = new PumpStreamHandler(adpater.getReasonerExecutionOutputStream(), adpater.getReasonerExecutionErrorStream(), adpater.getReasonerExecutionInputStream());
		
		// this is used to end the process when the JVM exits
		ShutdownHookProcessDestroyer processDestroyer = new ShutdownHookProcessDestroyer();
		
		// our main command executor
		mExecutor = new DefaultExecutor();
		
		// setting the properties
		mExecutor.setStreamHandler(streamHandler);
		mExecutor.setWatchdog(mWatchDog);
		 
		// setting the working directory
		mExecutor.setWorkingDirectory(new File(adpater.getReasonerExecutionWorkingDirectoryString()));
		mExecutor.setProcessDestroyer(processDestroyer);
		
		mStartReasonerExecutionTime = System.currentTimeMillis();
		
		// executing the command
		try {			
			mExecutor.execute(commandLine, new ExecuteResultHandler() {
				@Override
				public void onProcessComplete(int exitValue) {
					postEvent(new ReasonerProcessCompletedEvent(exitValue));					
				}
				@Override
				public void onProcessFailed(ExecuteException executionException) {
					postEvent(new ReasonerProcessExecutionExceptionEvent(executionException));
				}				
			});
			mLogger.info("Started execution of query '{}' for reasoner '{}'.", mQuery.toString(),mReasoner.toString());
			
		} catch (ExecuteException e) {
			postEvent(new ReasonerProcessExecutionExceptionEvent(e));
		} catch (IOException e) {
			postEvent(new ReasonerProcessExecutionIOExceptionEvent(e));
		}
		
		return true;
	}


	
}
