#!/bin/bash
# 
# OWL Reasoner Evaluation Workshop (ORE) 2013
# Example reasoner executor script
# Last updated: 27-Mar-13
# 
java -jar OREv2ReasonerWrapper.jar org.semanticweb.HermiT.Reasoner\$ReasonerFactory $*
		